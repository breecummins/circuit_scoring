from circuit_scoring import rank_order_run as rorun
from circuit_scoring import rank_order_stats as rostats
import sys


if __name__ == "__main__":
    helpstring = """Call with three required arguments and four optional arguments.
                    The first argument is the data file or a string representation of a list of data files with flow cytometry histograms.
                    The second argument is the metadata file or a string representation of a list of metadata files associated to the experiment(s) in the same order.
                    The third argument is the json configuration file (see README).
                    The fourth optional argument is a file name to save the output summary statistics dataframe.
                    The default is "stats_df.csv".
                    The fifth optional argument is a file name to save the experimental conditions for the circuits.
                    The default is "conditions.json".
                    The sixth optional argument is a file name to save the histograms used as controls.
                    The default is "controls.json".
                    The seventh optional argument is a file name to save the individual truth table scores.
                    This file is generally quite large. The default is "truth_table_scores.json".
    """
    if len(sys.argv) < 4:
        print(helpstring)
        sys.exit()

    datafile = sys.argv[1]
    metafile = sys.argv[2]
    config_json = sys.argv[3]

    save_fname = sys.argv[4] if len(sys.argv) >= 5 else "stats_df.csv"
    cond_fname = sys.argv[5] if len(sys.argv) >= 6 else "conditions.json"
    ctrl_fname = sys.argv[6] if len(sys.argv) >= 7 else "controls.json"
    tt_fname = sys.argv[7] if len(sys.argv) >= 8 else "truth_table_scores.json"

    scores, controls, conditions_per_circuit = rorun.run(datafile, metafile, config_json,cond_fname=cond_fname,ctrl_fname=ctrl_fname,tt_fname=tt_fname)
    rostats.make_stats_df(scores, config_json,savename=save_fname)

    print("Job complete.")