from circuit_scoring import rank_order_run as rorun
from circuit_scoring import rank_order_stats as rostats
import json

def test_etl():
    datafile = "YeastSTATES-1-0-Time-Series-Round-3-0__fc_etl_stats.csv"
    metafile = "YeastSTATES-1-0-Time-Series-Round-3-0__fc_meta.csv"
    config_json = "ys_round_trip_etl_BL1A.json"
    config = json.load(open(config_json))
    scores, controls, conditions = rorun.run(datafile, metafile, config)
    score_stats_df =  rostats.make_stats_df(scores,config,savename="statsdf1.csv")
    assert(len(score_stats_df) == 54)
    assert(len(conditions["AND"]) == 10)


# def test_rawlog10():
#     datafile = "YeastSTATES-1-0-Time-Series-Round-3-0__fc_raw_log10_stats.csv"
#     metafile = "YeastSTATES-1-0-Time-Series-Round-3-0__fc_meta.csv"
#     config_json = "ys_round_trip_rawlog10.json"
#     config = json.load(open(config_json))
#     scores, controls,conditions = rorun.run(datafile, metafile, config)
#     score_stats_df =  rostats.make_stats_df(scores,config,savename="statsdf2.csv")
#     assert(len(score_stats_df) == 74)
#     assert(len(conditions["AND"]) == 14)


def test_etl_multiple():
    datafile = ["YeastSTATES-1-0-Time-Series-Round-1__fc_etl_stats.csv","YeastSTATES-1-0-Time-Series-Round-3-0__fc_etl_stats.csv"]
    metafile = ["YeastSTATES-1-0-Time-Series-Round-1__fc_meta.csv","YeastSTATES-1-0-Time-Series-Round-3-0__fc_meta.csv"]
    config_json = "ys_round_trip_etl_BL1A.json"
    config = json.load(open(config_json))
    scores, controls,conditions = rorun.run(datafile, metafile, config)
    score_stats_df =  rostats.make_stats_df(scores,config,savename="statsdf2.csv")
    assert(len(score_stats_df) == 101)
    assert(len(conditions["AND"]) == 20)



if __name__ == '__main__':
    # test_etl()
    test_etl_multiple()