import numpy as np
from circuit_scoring.rank_order_truth_tables_lib import rank_truth_tables, rank_noncst_tables


def test1():
    # desired table = 0 1 1 1, clean data
    bin_vals = np.array([float(r) for r in range(12)])
    h1 = np.array([0., 1., 3., 5., 2., 0., 0., 0., 0., 0., 0., 0.])
    h2 = np.array([0., 0., 0., 0., 0., 0., 0., 0., 7., 3., 2., 0.])
    h3 = np.array([0., 0., 0., 0., 0., 0., 0., 2., 4., 1., 1., 0.])
    h4 = np.array([0., 0., 0., 0., 1., 1., 2., 8., 3., 0., 0., 0.])
    data = {"00" : h1, "01": h2, "10" : h3, "11" : h4}
    scores = rank_noncst_tables(data,bin_vals)
    assert({"00" : 0, "01" : 1, "10" : 1, "11" : 1} == scores[0][1])


def test2():
    # desired table = 0 0 1 1, but "10" is ambiguous
    bin_vals = np.array([float(r) for r in range(12)])
    h1 = np.array([1., 5., 6., 2., 0., 0., 0., 0., 0., 0., 0., 0.])
    h2 = np.array([0., 2., 5., 2., 1., 0., 0., 0., 0., 0., 0., 0.])
    h3 = np.array([0., 0., 0., 1., 3., 7., 2., 1., 0., 0., 0., 0.])
    h4 = np.array([0., 0., 0., 0., 0., 0., 2., 4., 5., 1., 0., 0.])
    data = {"00" : h1, "01": h2, "10" : h3, "11" : h4}
    scores = rank_noncst_tables(data,bin_vals)
    toptwo = [scores[0][1],scores[1][1]]
    assert({"00" : 0, "01" : 0, "10" : 1, "11" : 1} in toptwo)
    assert({"00" : 0, "01" : 0, "10" : 0, "11" : 1} in toptwo)


def test3():
    # desired table = 0 0 0 0, well-separated controls
    bin_vals = np.array([float(r) for r in range(12)])
    h1 = np.array([1., 5., 6., 2., 0., 0., 0., 0., 0., 0., 0., 0.])
    h2 = np.array([0., 2., 5., 2., 1., 0., 0., 0., 0., 0., 0., 0.])
    h3 = np.array([0., 3., 7., 2., 1., 0., 0., 0., 0., 0., 0., 0.])
    h4 = np.array([2., 4., 5., 1., 2., 0., 0., 0., 0., 0., 0., 0.])
    neg_control = np.array([3., 8., 1., 0., 0., 0., 0., 0., 0., 0., 0., 0.])
    pos_control = np.array([0., 0., 0., 0., 0., 0., 0., 0., 0., 3., 8., 1.])
    controls = {"pos" : pos_control, "neg" : neg_control}
    data = {"00" : h1, "01": h2, "10" : h3, "11" : h4}
    scores = rank_truth_tables(data,bin_vals,controls)
    assert({"00" : 0, "01" : 0, "10" : 0, "11" : 0} == scores[0][1])


def test4():
    # desired table = 0 0 0 0, close controls
    bin_vals = np.array([float(r) for r in range(12)])
    h1 = np.array([1., 5., 6., 2., 0., 0., 0., 0., 0., 0., 0., 0.])
    h2 = np.array([0., 2., 5., 2., 1., 0., 0., 0., 0., 0., 0., 0.])
    h3 = np.array([0., 3., 7., 2., 1., 0., 0., 0., 0., 0., 0., 0.])
    h4 = np.array([2., 4., 5., 1., 2., 0., 0., 0., 0., 0., 0., 0.])
    neg_control = np.array([0., 1., 6., 2., 0., 0., 0., 0., 0., 0., 0., 0.])
    pos_control = np.array([0., 0., 0., 0., 0., 3., 8., 1., 0., 0., 0., 0.])
    controls = {"pos" : pos_control, "neg" : neg_control}
    data = {"00" : h1, "01": h2, "10" : h3, "11" : h4}
    scores = rank_truth_tables(data,bin_vals,controls)
    assert({"00" : 0, "01" : 0, "10" : 0, "11" : 0} == scores[0][1])
    assert({"00" : 0, "01" : 1, "10" : 1, "11" : 0} == scores[1][1])


def test5():
    # desired table = 0 1 0 0, close controls
    bin_vals = np.array([float(r) for r in range(12)])
    h1 = np.array([1., 5., 6., 2., 0., 0., 0., 0., 0., 0., 0., 0.])
    h2 = np.array([0., 0., 0., 0., 2., 5., 2., 1., 0., 0., 0., 0.])
    h3 = np.array([0., 3., 7., 2., 1., 0., 0., 0., 0., 0., 0., 0.])
    h4 = np.array([2., 4., 5., 1., 2., 0., 0., 0., 0., 0., 0., 0.])
    neg_control = np.array([0., 1., 6., 2., 0., 0., 0., 0., 0., 0., 0., 0.])
    pos_control = np.array([0., 0., 0., 0., 0., 3., 8., 1., 0., 0., 0., 0.])
    controls = {"pos" : pos_control, "neg" : neg_control}
    data = {"00" : h1, "01": h2, "10" : h3, "11" : h4}
    scores = rank_truth_tables(data,bin_vals,controls)
    assert({"00" : 0, "01" : 1, "10" : 0, "11" : 0} == scores[0][1])
