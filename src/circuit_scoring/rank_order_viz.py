import ast, itertools
from circuit_scoring import rank_order_stats as ttstats
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
plt.rcParams.update({'font.size': 18})


def plot_stats(three_scores, yaxis = "rank",customized_labels=None,show_plot=True):
    # three_scores is the output of ttstats.get3scores
    # customized_labels is function handle
    # yaxis can be "rank" or "score"
    norm = mpl.colors.Normalize(vmin=0, vmax=8)
    rgba_color = [plt.cm.jet(norm(n)) for n in range(8)]
    for circuit, conditions in three_scores.items():
        conds = []
        for key, scrs in conditions.items():
            conds.append((key, *ttstats.make_stats(scrs)))
        analysis = sorted(conds)
        xlabels, ranks, scores, firstplace, num_reps = zip(*analysis)
        tups = [ast.literal_eval(x) for x in xlabels]
        if customized_labels:
            xlabels = customized_labels(tups)
        else:
            xlabels = [x[:20] + "..." for x in xlabels]
        repeat_colors = list([len(list(g)) for k, g in itertools.groupby(tups, key=lambda x: x[0])])
        cmaplist = rgba_color * int(np.ceil(len(repeat_colors)/len(rgba_color)))
        cum_colors = [0] + list(np.cumsum(repeat_colors))[:-1]
        color_info = zip(cum_colors, repeat_colors, cmaplist)
        if yaxis == "rank":
            means = [r[0] for r in ranks]
            stds = [r[1] for r in ranks]
            title = circuit + " truth table rank (1-9)"
            ylabel = "Mean rank, lower is better"
            top = 10
        elif yaxis == "score":
            means = [r[0] for r in scores]
            stds = [r[1] for r in scores]
            title = circuit + " truth table score (0-1)"
            ylabel = "Mean score, higher is better"
            top = 1.05
        else:
            raise ValueError("Type score not recognized in yaxis. Choose either 'rank' or 'score'.")
        make_bar(means, stds, xlabels, color_info, title, ylabel, [0,top], show_plot)


def make_bar(means,stds,labels,color_info,title,ylabel,ylim,show_plot):
    plt.figure()
    for i, (k, r, c) in enumerate(color_info):
        if stds:
            plt.bar(range(k, k + r), means[k:k + r], yerr=stds[k:k + r], color=[c] * r, alpha=0.5)
        else:
            plt.bar(range(k, k + r), means[k:k + r], color=[c] * r, alpha=0.5)
    plt.title(title)
    plt.ylabel(ylabel)
    plt.ylim(ylim)
    plt.xticks(rotation=70)
    ticks = range(-1, len(labels) + 1)
    plt.xlim = [ticks[0], ticks[-1]]
    plt.locator_params(axis='x', nbins=len(labels) + 1)
    plt.xticks(ticks, [None] + list(labels) + [None])
    if show_plot:
        plt.show()


def plot_stats_df(stats_df, circuit_column, group_cols, stats_col,top=None,customized_labels=None,show_plot=True,savename_start=False):
    # make a bar graph from a pandas dataframe
    #
    # stats_df is the output dataframe of ttstats.make_stats_df
    # circuit_column is the column name of stats_df that has the circuit name in it
    # group_cols is a list of column names of stats_df that will be grouped together into conditions
    # stats_col is the column name of stats_df that has the statistic to be plotted in a bar graph, will either have "rank" or "score" in the name
    # top is the highest y-value of the score or rank (lowest is always zero)
    # customized_labels is a function handle for renaming the grouped columns to shorten them for plotting
    # show_plot is obvious
    # savename_start will have the circuit appended to the end to save the figures, default is don't save

    circuits = list(stats_df[circuit_column].unique())
    for circuit in circuits:
        circuit_df = stats_df.loc[stats_df[circuit_column] == circuit]
        if stats_col.startswith("mean"):
            err = ["std" + stats_col[4:]]
        else:
            err = []
        circuit_df.drop(circuit_df.columns.difference(group_cols + [stats_col] + err),axis=1,inplace=True)
        grouped = circuit_df.groupby(group_cols)
        if stats_col.startswith("mean"):
            gp_data = sorted([(name,list(group[stats_col]),list(group[err[0]])) for name,group in grouped])
            xlabels,vals, err_vals = zip(*gp_data)
            yerr = [v[0] for v in err_vals]
        else:
            gp_data = sorted([(name,list(group[stats_col])) for name,group in grouped])
            xlabels,vals = zip(*gp_data)
            yerr = None
        if customized_labels:
            xlabels = customized_labels(xlabels)
        else:
            xlabels = [x[:20] + "..." for x in xlabels]
        y = [v[0] for v in vals]
        if "rank" in stats_col:
            title = circuit + " truth table rank (1-9)"
            ylabel = "Mean rank, lower is better"
            if not top:
                top = 10
        elif "score" in stats_col:
            title = circuit + " truth table score (0-1, mod flanking controls)"
            ylabel = "Mean score, higher is better"
            if not top:
                top = 1.05
        else:
            title = circuit + " {}".format(stats_col)
            ylabel = stats_col
        if savename_start:
            savename = savename_start+"_{}.jpg".format(circuit)
        else:
            savename = ""
        make_bar_df(y, yerr, xlabels, title, ylabel, [0,top], show_plot, savename)


def make_bar_df(means,stds,labels,title,ylabel,ylim,show_plot,savefig_fname=""):
    fig = plt.figure()
    plt.bar(range(len(means)), means, yerr=stds, alpha=0.5)
    plt.title(title)
    plt.ylabel(ylabel)
    plt.ylim(ylim)
    plt.xticks(rotation=70)
    ticks = range(-1, len(labels) + 1)
    plt.xlim = [ticks[0], ticks[-1]]
    plt.locator_params(axis='x', nbins=len(labels) + 1)
    plt.xticks(ticks, [None] + list(labels) + [None])
    if savefig_fname:
        plt.savefig(savefig_fname, bbox_inches="tight", dpi=300)
    if show_plot:
        plt.show()


def customize_xlabels_media(label_as_tuple):
    clabels = []
    for label in label_as_tuple:
        if "Ethanol" in label[0]:
            clabels.append("Eth, " + str(label[1]))
        elif "Sorbitol" in label[0]:
            clabels.append("Sorb, " + str(label[1]))
        elif "YEP" in label[0]:
            clabels.append("YEP, " + str(label[1]))
        else:
            clabels.append("SC, " + str(label[1]))
    return clabels


plot_stats_df_old = plot_stats_df
