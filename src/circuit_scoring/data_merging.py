import pandas as pd
import math
import numpy as np
import ast, numbers


def get_histograms(df,join_on):
    ids = df[join_on].to_list()
    bin_vals = [float(x.split("_")[1]) for x in df.columns if "bin" in x]
    df = drop_and_keep_cols(df,[],[x for x in df.columns if "bin" in x])
    histograms = df.to_numpy()
    new_df = []
    for k,s in enumerate(ids):
        new_df.append({join_on: s, "histogram" : histograms[k,:]})
    return pd.DataFrame.from_records(new_df), bin_vals


def drop_and_keep_cols(df,drop_cols,keep_cols):
    if drop_cols:
        df = df.drop(drop_cols, axis=1, inplace=False)
    if keep_cols:
        df = df.drop(df.columns.difference(keep_cols), axis=1, inplace=False)
    return df


def transform_columns(df,transformations):
    # currently only supports log transformation
    for op,cols in transformations.items():
         if op == "log10":
             for col in cols:
                df[col] = df[col].apply(math.log10)
         else:
             raise ValueError("Transformation operation {} needs to be implemented.".format(op))
    return df


def filter_df(df,filter_cols):
    for col,val in filter_cols.items():
        desired_value = val[0]        
        if isinstance(desired_value,numbers.Number):
            df[col] = pd.to_numeric(df[col])
            if not pd.api.types.is_numeric_dtype(df[col]):
                raise ValueError("Comparing numeric value to non-numeric value in column {}.".format(col))
        if val[1] == "eq":
            if isinstance(desired_value,numbers.Number):
                # np.isclose handles float comparison
                df = df.loc[np.isclose(df[col], desired_value)]
            else:
                df = df.loc[df[col] == desired_value]
        elif val[1] == "gteq":
            df = df.loc[df[col].ge(desired_value) | np.isclose(df[col], desired_value)]
        elif val[1] == "lteq":
            df = df.loc[df[col].le(desired_value) | np.isclose(df[col], desired_value)]
        elif val[1] == "in_list":
            if isinstance(desired_value,numbers.Number):
                df = df.loc[any([np.isclose(df[col],d) for d in desired_value])]
            else:
                df = df.loc[df[col].isin(desired_value)]
        else:
            raise ValueError("Operator {} not recognized in filtering_criteria for column {}".format(val[1],col))
    return df


def merge_files(files):
    # merge files into one df
    # files may be a single file name, a list of file names, or a string representation of a list of file names
    if isinstance(files, str) and "[" in files:
        files = ast.literal_eval(files)
        if not isinstance(files,list):
            raise ValueError("Data file type must be string or list of strings.")
    if isinstance(files,list):
        df = pd.read_csv(files[0])
        for f in files[1:]:
            dd = pd.read_csv(f)
            df = pd.concat([df,dd],sort=True)
    elif isinstance(files,str) and "[" not in files:
        df = pd.read_csv(files)
    else:
        raise ValueError("Data file type must be string or list of strings.")
    return df


def data_meta_merge(datafile,metafile,config):
    # datafile is the file name of a .csv with flow cytometry data that is summarized into a histogram in each row.
    # Alternatively, it is a list of such file names, or a string representation of such a list.
    # Each datafile must contain "bin_#" columns indicating the bin values of the histogram.
    # The geometric mean of the histogram must also be a column.
    #
    # The metafile is the file name of a metadata .csv associated to the datafile that can be joined over a unique identifier column.
    # Alternatively, it is a list of such file names, or a string representation of such a list.
    # Every unique identifier must exist in both a datafile and a metafile.
    #
    # config_json may be either a dictionary or a json file containing the following five dictionaries:
    # file_columns, transformations, filtering_criteria, control_params, and circuit_params
    #
    # file_columns = {
    #              "data_file_drop_cols" : column_name_list,
    #              "data_file_keep_cols" : column_name_list,
    #              "meta_file_drop_cols" : column_name_list,
    #              "meta_file_keep_cols" : column_name_list,
    #              "join_on" : column_name
    #              }
    # Any column list above may be empty or False.
    # If a drop column list is empty or False, then no columns will be dropped.
    # If a keeper column list is empty or False, then every column will be kept.
    #
    # "transformations" = {
    #     "log10" : ["geo_mean"]
    # }
    # is a dictionary with operations keying lists of column names. Currently only log10 transformations are implemented.
    #
    # filtering_criteria (optional) = {
    #                       "column_name1" : (desired_column_value, operator),
    #                       "column_name2" : (desired_column_value, operator),
    #                        ... }
    # The parameter operator is one of "eq" (equals), "gteq" (greater than or equal to), "lteq" (less than or equal to), or "in_list" (equal to anything in a list).
    # This dictionary will generally have the "channel" column for flow cytometry data.
    # If the parameter is missing, empty, or False, no filtering will take place.
    #
    # NOTE: Transformations occur BEFORE filtering.
    #

    print("Merging files.")
    # construct data df
    data_df = merge_files(datafile)
    drop_cols = config["file_columns"]["data_file_drop_cols"]
    keep_cols = config["file_columns"]["data_file_keep_cols"]
    data_df = drop_and_keep_cols(data_df,drop_cols,keep_cols)
    # construct meta data df
    merge_df = merge_files(metafile)
    drop_cols = config["file_columns"]["meta_file_drop_cols"]
    keep_cols = config["file_columns"]["meta_file_keep_cols"]
    merge_df = drop_and_keep_cols(merge_df,drop_cols,keep_cols)
    # merge the two dfs
    data_df = data_df.merge(merge_df,on=config["file_columns"]["join_on"],how="inner")
    # transform and filter merged df
    data_df = transform_columns(data_df, config["transformations"])
    data_df = filter_df(data_df,config["filtering_criteria"])
    # extract the histograms from the data
    hist_df, bin_vals = get_histograms(data_df,config["file_columns"]["join_on"])
    # remove individual bin values and replace with a column for the whole histogram
    data_df = data_df.drop([x for x in data_df.columns if "bin" in x], axis =1, inplace=False)
    data_df = pd.merge(hist_df, data_df, on=config["file_columns"]["join_on"],how="inner")
    # save result just in case
    data_df.to_csv("rank_order_truth_tables_data_file.csv",index=False)
    return data_df, bin_vals

