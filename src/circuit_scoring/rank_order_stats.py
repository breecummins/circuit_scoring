from circuit_scoring import rank_order_run as ttrun
import ast,json,os,sys
import numpy as np
import pandas as pd


def load_file(fname):
    try:
        json_dict = json.load(open(fname))
    except:
        json_dict = fname
    return json_dict


def strip_nans(scores):
    no_nans = {}
    for cond,scrs in scores.items():
        no_nans[cond] = []
        for combo in scrs:
            no_nans[cond].append([c for c in combo if not np.isnan(c[0])])
    return no_nans


def get_ground_truth(key,possible_circuits=["XNOR", "NAND", "NOR", "XOR", "AND", "OR"]):
    ground_truth = None
    for circuit in possible_circuits:
        if circuit in key:
            ground_truth = ttrun.get_truth_table_from_circuit(circuit)
            break
    return ground_truth


def firstranks(scrs, ground_truth):
    # scrs is a list of length 8 lists of (score,truth table) tuples,
    # one list for each replicate of a circuit+condition
    # it is possible to have an empty list instead of length 8
    # these are filtered out in the if ss: line
    scores = [0, 0]
    for ss in scrs:
        if ss:
            scores[1] += 1
            top = ss[0][1]
            if top == ground_truth:
                scores[0] += 1
    return scores


def ranks_and_scores(scrs, ground_truth):
    # scrs is a list of length 8 lists of (score,truth table) tuples,
    # one list for each replicate of a circuit+condition
    # it is possible to have an empty list instead of length 8
    # these are filtered out in the if ss: line
    ranks = []
    scores = []
    for ss in scrs:
        if ss:
            found = False
            for rank, s in enumerate(ss):
                if s[1] == ground_truth:
                    ranks.append(rank + 1)
                    scores.append(s[0])
                    found = True
                    break
            if not found:
                ranks.append(len(ss) + 1)
                scores.append(0)
    return ranks, scores


def get3scores(scores):
    # scores comes from load_scores(), either from a json file or the ttrun.run() function
    scrdict = {}
    for key, scrs in scores.items():
        if scrs and any([s for s in scrs]):
            ground_truth = get_ground_truth(key)
            if ground_truth:
                rank1 = firstranks(scrs, ground_truth)
                ranks, scores = ranks_and_scores(scrs, ground_truth)
                scrdict[key] = {"rank1": rank1, "ranks": ranks, "scores": scores}
    return scrdict


def make_stats(scrs):
    rank1scrs = scrs["rank1"]
    rankscrs = scrs["ranks"]
    scores = scrs["scores"]
    ranks_stat = (np.mean(rankscrs), np.std(rankscrs))
    score_stat = (np.mean(scores), np.std(scores))
    firstplace = rank1scrs[0]/rank1scrs[1]
    num_reps = rank1scrs[1]
    return ranks_stat, score_stat, firstplace, num_reps


def make_stats_df(score_file,config_file,savename="stats_df.csv"):
    # arguments may be file names OR dictionaries
    config = load_file(config_file)
    if config["control_params"]["circuit_col"] not in config["circuit_params"]["group_cols"]:
        group_cols = [config["control_params"]["circuit_col"]] + config["circuit_params"]["group_cols"]
    else:
        group_cols = config["circuit_params"]["group_cols"]
    column_names = group_cols + ["prop_first_place","num_reps", "mean_rank", "std_rank", "mean_cut_score", "std_cut_score"]
    scores = load_file(score_file)
    scores = strip_nans(scores)
    three_scores = get3scores(scores)
    score_stats = []
    for key,scrs in three_scores.items():
        if isinstance(key,str):
            cols = list(ast.literal_eval(key))
        else:
            cols = list(key)
        (rankmean,rankstd),(scoremean,scorestd),firstplace, num_reps = make_stats(scrs)
        score_stats.append(cols + [firstplace,num_reps,rankmean,rankstd,scoremean,scorestd])
    if not score_stats:
        print("Warning: no data in {}.".format(savename))
        open(savename,"w").write("")
        return None
    else:
        score_df = pd.DataFrame(score_stats,columns=column_names)
        score_df.to_csv(savename,index=False)
        return score_df


def make_truth_tables_df(score_file,savename="truth_table_all_ranks.json"):
    all_circuits = ["AND", "OR", "NAND", "NOR", "XOR", "XNOR", "C_IMPLY", "M_IMPLY", "C_NIMPLY", "M_NIMPLY", "TRUE", "FALSE", "OTHER1", "NOTHER1", "OTHER2", "NOTHER2"]
    scores = load_file(score_file)
    scores = strip_nans(scores)
    ranks = {key : dict(zip(all_circuits,[0]*len(all_circuits))) for key in scores.keys()}
    for key, scrs in scores.items():
        if scrs and any([s for s in scrs]):
            for s in scrs:
                if s:
                    circuit = ttrun.get_circuit_from_truth_table(s[0][1])
                    ranks[key][circuit] +=1
    json.dump(ranks,open(savename,"w"))
    return ranks



if __name__ == "__main__":

    if len(sys.argv) < 5:
        # df,cond = make_stats_df(sys.argv[1], sys.argv[2], sys.argv[3])
        ranks = make_truth_tables_df(sys.argv[1])
    else:
        # df, cond = make_stats_df(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
        ranks = make_truth_tables_df(sys.argv[1])


