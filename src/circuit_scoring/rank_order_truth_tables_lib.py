# The MIT License (MIT)
#
# Copyright (c) 2018 Bree Cummins
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import pyemd, itertools
import numpy as np


def emdist(h1, h2, bin_vals):
    '''
    Calculate earth mover's distance between 2 histograms. Histograms are normalized to mass 1.

    :param h1: a 1-D numpy array representing a histogram with the bin values used to make bin_dist
    :param h2: a 1-D numpy array representing a histogram with the bin values used to make bin_dist
    :param bin_vals: representative points in the bins defining the histograms in data
    :return: a scalar value that is the earth mover's distance between normalized h1 and h2
    '''
    bin_dist = np.array([[np.abs(m - n) for m in bin_vals] for n in bin_vals])
    if float(sum(h1)) > 0 and float(sum(h2)) > 0 and not any(np.isnan(h1)) and not any(np.isnan(h1)):
        return pyemd.emd(np.asarray(h1)/float(sum(h1)), np.asarray(h2)/float(sum(h2)), bin_dist)
    else:
        return np.nan


def make_graph(data,bin_vals):
    '''
    Make a dictionary of pairwise earth-mover's distances that represents a weighted graph.

    :param data: a length 2^k dictionary mapping an input state to a 1-D numpy array histogram, all defined by the same bin values
    :param bin_vals: representative points in the bins defining the histograms in data
    :return: dictionary of edges to edge weights
    '''
    inputstates = [k for k in data]
    graph = {}
    for t,i in enumerate(inputstates[:-1]):
        for j in inputstates[t+1:]:
             graph.update( {(i,j) : emdist(data[i],data[j],bin_vals) } )
    return graph


def calculate_partitions(inputstates):
    '''
    Calculate all ways to partition a list into 2 nonempty sets, where the list has an even number of elements.

    :param inputstates: a list with an even number of elements, N
    :return: a length 2^(N-1) - 1 list of lists, where each sublist represents half of a partition (the other half is deduced by inputstates \ sublist)
    '''
    N = len(inputstates)
    partitions = []
    for k in range(1,int(N/2)+1):
        combs = [c for c in itertools.combinations(inputstates, k)]
        partitions.extend(combs)
    # there is double-counting of partitions at the median value
    partitions = partitions[:-int(len(combs)/2)]
    return partitions


def averaged_cut(p, graph, scaleby):
    '''
    Calculate averaged cut score for the partition and inputstates \ partition.

    :param p: a list or set containing a subset of inputstates
    :param graph: a dictionary that is the output of make_graph()
    :param scaleby: float representing distance between positive and negative controls (or 1.0 for missing controls)
    :return: a scalar value
    '''
    cut_set = [v for (i,j),v in graph.items() if (i in p) != (j in p)]
    return sum(cut_set) / len(cut_set) / scaleby


def mean_hist(hist,bin_vals):
    '''
    Return the mean of a histogram.

    :param hist: an iterable containing the counts in each bin
    :param bin_vals: an iterable containing a representative value for each bin
    :return: mean value of the histogram
    '''
    return sum([x*h for (x,h) in zip(bin_vals,hist)]) / float(sum(hist))


def assign_0or1(partition,data,bin_vals):
    '''
    Figure out if a partition should be assigned 1 (higher valued collection of histograms) or 0 (lower valued)

    :param partition: a list or set containing a subset of the keys in data
    :param data: a length 2^k dictionary mapping an input state to a 1-D numpy array histogram, all defined by the same bin values
    :param bin_vals: representative points in the bins defining the histograms in data
    :return: 0 or 1
    '''
    hp = sum([h for k, h in data.items() if k in partition])
    hq = sum([h for k, h in data.items() if k not in partition])
    mp = mean_hist(hp, bin_vals)
    mq = mean_hist(hq, bin_vals)
    return int(mq < mp)


def make_truth_table(partition,inputstates,b):
    '''
    Return the truth table for a partition.

    :param partition: an iterable containing a subset of inputstates
    :param inputstates: a list of length N
    :param b: 0 or 1
    :return: a dictionary of input states keying Boolean values
    '''
    return dict([(i,b) if i in partition else (i, (b + 1) % 2) for i in inputstates])


def rank_noncst_tables(data,bin_vals,controls=None):
    '''
    Return scored non-constant truth tables.

    :param data: a length 2^k dictionary mapping an input state to a 1-D numpy array histogram, all defined by the same bin values
    :param bin_vals: representative points in the bins defining the histograms in data
    :param controls: (optional) dictionary with histograms for positive (key = "pos") and negative (key = "neg") controls.
                    Used to scale data and assess constant truth tables
    :return: list of tuples of a real valued averaged cut score with its associated truth table. Larger scores are better.
    '''
    scaleby = emdist(controls["pos"], controls["neg"], bin_vals) if controls else 1.0
    graph = make_graph(data,bin_vals)
    inputstates = [k for k in data]
    partitions = calculate_partitions(inputstates)
    scores = []
    for p in partitions:
        truthtable = make_truth_table(p,inputstates,assign_0or1(p,data,bin_vals))
        scores.append((averaged_cut(p, graph,scaleby), truthtable))
    return sorted(scores, key=lambda x : x[0], reverse=True)


def rank_truth_tables(data,bin_vals,controls):
    '''
    Return scored non-constant and constant truth tables.

    :param data: a length 2^k dictionary mapping an input state to a 1-D numpy array histogram, all defined by the same bin values
    :param bin_vals: representative points in the bins defining the histograms in data
    :param controls: dictionary with histograms for positive (key = "pos") and negative (key = "neg") controls.
                    Used to scale data and assess constant truth tables
    :return: list of tuples of a real valued averaged cut score with its associated truth table. Larger scores are better.
    '''
    scores = rank_noncst_tables(data,bin_vals,controls)
    top_score = sorted(scores, key=lambda x : x[0], reverse=True)[0][0]
    scores.append(assess_cst_truthtable(top_score,data,bin_vals,controls))
    return sorted(scores, key=lambda x : x[0], reverse=True)


def assess_cst_truthtable(top_score,data,bin_vals,controls):
    '''
    Return a score for the constant truth table

    :param top_score: best score for a nonconstant truth table
    :param data: see rank_truth_tables
    :param bin_vals: see rank_truth_tables
    :param controls: see rank_truth_tables
    :return: a tuple of the score for and the choice of a constant truth table
    '''

    # need to get 0 or 1
    dist_pos = 0
    dist_neg = 0
    for key, h in data.items():
        dist_neg += emdist(h, controls["neg"], bin_vals)
        dist_pos += emdist(h, controls["pos"], bin_vals)
    val = int(dist_neg > dist_pos)
    # The formula below was empirically determined. There's no theoretical reason for it, except that top_score <= 1 provided the positive and negative controls flank all the data. The power of 4 is arbitrary, and prevents the constant truth tables from always dominating the results.
    return (1 - top_score)**4, dict([(i,val) for i in data.keys()])

