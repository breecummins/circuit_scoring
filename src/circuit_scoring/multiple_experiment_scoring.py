import pandas as pd
from numpy import inf,sqrt
import json,sys


def read_files(list_of_score_files,list_of_condition_files):
    dfs = [pd.read_csv(fname) for fname in list_of_score_files]
    conditions = [json.load(open(fname)) for fname in list_of_condition_files]
    return dfs, conditions


def circuit_ranking_across_conditions(dfs,conditions,circuit_col,group_cols,circuit_names):
    circuits = {c : [[],[],[]] for c in circuit_names}
    circuit_indices = {c : set([]) for c in circuit_names}
    for df in dfs:
        for circuit in circuits:
            df2 = df.loc[df[circuit_col]==circuit]
            df3 = df2.groupby(group_cols).size().reset_index(name='counts')
            inds = list(zip(*[list(df3[col].values) for col in group_cols]))
            circuit_indices[circuit]=circuit_indices[circuit].union(inds)
        for row in df.itertuples():
            circuits[row.strain_circuit][0].append(row.mean_rank)
            circuits[row.strain_circuit][1].append(row.std_rank)
            circuits[row.strain_circuit][2].append(row.num_reps)
    circuit_scores = []
    for circuit,vals in circuits.items():
        meanranks = vals[0]
        if meanranks:
            nc = len(circuit_indices[circuit])
            tot = len(conditions[circuit])
            # normalize by the number of conditions where the circuit had any measurements taken
            norm_nc = nc/tot
            numreps = vals[2]
            total = sum(numreps)
            score = sum([m*n for m,n in zip(meanranks,numreps)])/total
            std = combine_std(vals[1],meanranks,numreps,score)
        else:
            norm_nc = 0
            score = inf
            std = 0
            nc = 0
            tot = len(conditions[circuit])
        circuit_scores.append((circuit,norm_nc,score,std,nc,tot))
    circuits,successes,scores,stds,num_conds,tot_conds = zip(*circuit_scores)
    circuit_df = pd.DataFrame()
    circuit_df["circuit"] = circuits
    circuit_df["mean rank (1-9)"] = scores
    circuit_df["std rank"] = stds
    circuit_df["prop. exp. conds. with growth"] =  successes
    circuit_df["exp. conds / mean rank"] = [s / m for (s,m) in zip(successes,scores)]
    circuit_df["conds. with growth in 4 input states"] = num_conds
    circuit_df["total # conds. with growth"] = tot_conds
    return circuit_df


def combine_std(stds,means,num_samp,total_mean):
    variance = sum([(n-1)*s**2 + n*(m - total_mean) for s,m,n in zip(stds,means,num_samp)])
    total_samp = sum(num_samp)
    return sqrt(variance / (total_samp - 1))


def aggregate_experimental_conditions(conditions):
    # combine the list of condition dictionaries into one dictionary keyed by circuit
    # this dict contains the collection of experimental conditions under which at least one strain showed growth
    conditions_aggregate = {}
    for cond_dict in conditions:
        for circuit,cond in cond_dict.items():
            cond = [tuple(c) for c in cond]
            if circuit not in conditions_aggregate:
                conditions_aggregate[circuit] = set(cond)
            else:
                conditions_aggregate[circuit] = conditions_aggregate[circuit].union(cond)
    return conditions_aggregate


def get_circuit_rankings(df_fnames,condition_fnames,circuit_col = "strain_circuit",group_cols=["media_type","timepoint","temperature"],circuit_names=["AND", "NAND","OR","NOR","XOR","XNOR"]):
    dfs, conditions = read_files(df_fnames,condition_fnames)
    conditions_aggregate = aggregate_experimental_conditions(conditions)
    circuit_df = circuit_ranking_across_conditions(dfs, conditions_aggregate, circuit_col, group_cols, circuit_names)
    return circuit_df, conditions_aggregate


if __name__ == "__main__":
    if len(sys.argv) <= 2:
        print("First required argument: list of file names of stats dataframes.\n"
              "Second required argument: corresponding list of condition file names.\n"
              "These files are produced by rank_order_stats.make_df.\n")
        sys.exit()
    df_fnames = sys.argv[1]
    condition_fnames = sys.argv[2]
    if len(sys.argv) ==3:
        circuit_df, conditions = get_circuit_rankings(df_fnames,condition_fnames)
    if len(sys.argv) >=4:
        circuit_col = sys.argv[3]
        if len(sys.argv) == 4:
            circuit_df, conditions = get_circuit_rankings(df_fnames,condition_fnames,circuit_col)
    if len(sys.argv) >=5:
        group_cols=sys.argv[4]
        if len(sys.argv) == 5:
            circuit_df, conditions = get_circuit_rankings(df_fnames,condition_fnames,circuit_col,group_cols)
    if len(sys.argv) >= 6:
        circuit_names = sys.argv[5]
        circuit_df, conditions = get_circuit_rankings(df_fnames,condition_fnames,circuit_col,group_cols,circuit_names)






