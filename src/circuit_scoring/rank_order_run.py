import circuit_scoring.rank_order_truth_tables_lib as rott
import circuit_scoring.data_merging as datamerge
import itertools, json, sys, os
from multiprocessing import Pool
from functools import partial


def separate_controls(df,control_params):
    pos = control_params["pos"]
    neg = control_params["neg"]
    control_col = control_params["control_col"]
    circuit_col = control_params["circuit_col"]
    pos_control_df = df.loc[df[control_col] == pos]
    pos_control_df = pos_control_df.drop([circuit_col],axis=1,inplace=False)
    neg_control_df = df.loc[df[control_col] == neg]
    neg_control_df = neg_control_df.drop([circuit_col],axis=1,inplace=False)
    circuit_df = df.loc[df[circuit_col] != ""]
    circuit_df = circuit_df.dropna(subset=[circuit_col])
    circuit_df = circuit_df.drop([control_col],axis=1,inplace=False)
    return pos_control_df, neg_control_df, circuit_df


def extract_circuits(df,circuit_params,circuit_col):
    group_cols = circuit_params["group_cols"]
    if circuit_col not in group_cols:
        group_cols = [circuit_col] + group_cols
    input_states = circuit_params["input_states"]
    input_col = circuit_params["input_col"]
    grouped_df = df.groupby(group_cols)
    circuits = {name : [] for name,_ in grouped_df}
    for name,group in grouped_df:
        histograms = []
        for state in input_states:
            g = group.loc[group[input_col] == int(state)]
            histograms.append(g["histogram"].to_list())
        for hist_list in itertools.product(*histograms):
            ct = {instate : h for instate,h in zip(input_states,hist_list)}
            circuits[name].append(ct)
    return circuits


def get_conditions(df, circuit_column, groupby_columns):
    df = df.drop([circuit_column],axis=1)
    grpby_cols = groupby_columns[:]
    if circuit_column in grpby_cols:
        grpby_cols.remove(circuit_column)
    g = df.groupby(grpby_cols).count()
    return list(g.index)


def get_conditions_per_circuit(df, circuit_column, groupby_columns):
    grpby_cols = groupby_columns[:]
    if circuit_column in grpby_cols:
        grpby_cols.remove(circuit_column)
    circuits = df[circuit_column].dropna().unique()
    circuit_conditions = {}
    for circuit in circuits:
        dg = df.loc[df[circuit_column] == circuit].groupby(grpby_cols)
        g = dg.count()
        circuit_conditions[circuit] = list(g.index)
    return circuit_conditions


def choose_best_controls_wasserstein(pos_df,neg_df,join_on_col,geo_mean_col,geo_mean_log10,bin_vals):
    pos = zip(pos_df[join_on_col].to_list(),pos_df[geo_mean_col].to_list())
    neg = zip(neg_df[join_on_col].to_list(),neg_df[geo_mean_col].to_list())
    distances = []
    for (idpos,gmpos),(idneg,gmneg) in itertools.product(pos,neg):
        diff = gmpos - gmneg
        if diff >0:
            if geo_mean_log10:
                # base raised to the difference in exponents
                fold_change = 10**diff
            else:
                # ratio positive / negative
                fold_change = gmpos/gmneg
            phist = pos_df.loc[pos_df[join_on_col] == idpos]["histogram"].values
            nhist = neg_df.loc[neg_df[join_on_col] == idneg]["histogram"].values
            dist = rott.emdist(phist[0],nhist[0],bin_vals)
            distances.append((idpos,idneg,dist,fold_change))
    if not distances:
         return "All positive controls have a lower mean than all negative controls."
    best = max(distances,key=lambda x : (x[2],x[3]))
    pos_hist = pos_df.loc[pos_df[join_on_col] == best[0]]["histogram"].values
    neg_hist = neg_df.loc[neg_df[join_on_col] == best[1]]["histogram"].values
    return {"pos" : pos_hist[0], "neg" : neg_hist[0], "pos_id" : best[0], "neg_id" : best[1], "em_dist" : best[2], "fold_change" : best[3]}
    

def score_circuits(circuits,bin_vals,controls):
    scores = {name : [] for name in circuits}
    for group,cts in circuits.items():
        for c in cts:
            scores[group].append(rott.rank_truth_tables(c,bin_vals,controls))
    return scores


def score_circuit(bin_vals,controls,c):
    return rott.rank_truth_tables(c,bin_vals,controls)


def score_circuits_parallel(circuits,bin_vals,controls):
    scores = {name : [] for name in circuits}
    pool = Pool()
    pool_map = partial(score_circuit,bin_vals,controls)
    for group,cts in circuits.items():
        output = pool.map(pool_map,cts)
        scores[group] = list(output)
    # trying to avoid "Can't allocate memory" error
    pool.close()
    pool.join()    
    return scores


def get_truth_table_from_circuit(circuit):
    circuit_key = {
        "AND" : {'00': 0, '01': 0, '10': 0, '11': 1},
        "NAND" : {'00': 1, '01': 1, '10': 1, '11': 0},
        "OR" : {'00': 0, '01': 1, '10': 1, '11': 1},
        "NOR": {'00': 1, '01': 0, '10': 0, '11': 0},
        "XOR": {'00': 0, '01': 1, '10': 1, '11': 0},
        "XNOR": {'00': 1, '01': 0, '10': 0, '11': 1},
        "M_IMPLY": {'00': 1, '01': 1, '10': 0, '11': 1},
        "M_NIMPLY": {'00': 0, '01': 0, '10': 1, '11': 0},
        "C_IMPLY": {'00': 1, '01': 0, '10': 1, '11': 1},
        "C_NIMPLY": {'00': 0, '01': 1, '10': 0, '11': 0},
        "TRUE": {'00': 1, '01': 1, '10': 1, '11': 1},
        "FALSE": {'00': 0, '01': 0, '10': 0, '11': 0},
        "OTHER1": {"00": 0, "01": 1, "10": 0, "11": 1},
        "NOTHER1": {"00": 1, "01": 0, "10": 1, "11": 0},
        "OTHER2": {"00": 0, "01": 0, "10": 1, "11": 1},
        "NOTHER2": {"00": 1, "01": 1, "10": 0, "11": 0}
    }
    return circuit_key[circuit] if circuit in circuit_key else circuit


def get_circuit_from_truth_table(truth_table):
    circuit_key = {
        "AND" : {'00': 0, '01': 0, '10': 0, '11': 1},
        "NAND" : {'00': 1, '01': 1, '10': 1, '11': 0},
        "OR" : {'00': 0, '01': 1, '10': 1, '11': 1},
        "NOR": {'00': 1, '01': 0, '10': 0, '11': 0},
        "XOR": {'00': 0, '01': 1, '10': 1, '11': 0},
        "XNOR": {'00': 1, '01': 0, '10': 0, '11': 1},
        "M_IMPLY": {'00': 1, '01': 1, '10': 0, '11': 1},
        "M_NIMPLY": {'00': 0, '01': 0, '10': 1, '11': 0},
        "C_IMPLY": {'00': 1, '01': 0, '10': 1, '11': 1},
        "C_NIMPLY": {'00': 0, '01': 1, '10': 0, '11': 0},
        "TRUE": {'00': 1, '01': 1, '10': 1, '11': 1},
        "FALSE": {'00': 0, '01': 0, '10': 0, '11': 0},
        "OTHER1": {"00": 0, "01": 1, "10": 0, "11": 1},
        "NOTHER1": {"00": 1, "01": 0, "10": 1, "11": 0},
        "OTHER2": {"00": 0, "01": 0, "10": 1, "11": 1},
        "NOTHER2": {"00": 1, "01": 1, "10": 0, "11": 0}
    }
    for circuit, tt in circuit_key.items():
        if tt == truth_table:
            return circuit
    return truth_table


def run(datafile, metafile, config_json, tt_fname = "truth_table_scores.json",cond_fname="conditions.json",ctrl_fname="controls.json"):
    # datafile is the file name of a .csv with flow cytometry data that is summarized into a histogram in each row.
    # The datafile must contain "bin_#" columns indicating the bin values of the histogram.
    # The geometric mean of the histogram must also be a column.
    #
    # The metafile is a meta data file associated to the datafile that can be joined over a unique identifier column.
    #
    # config_json may be either a dictionary or a json file containing the following five dictionaries:
    # file_columns, transformations, filtering_criteria, control_params, and circuit_params
    #
    # file_columns = {
    #              "data_file_drop_cols" : column_name_list,
    #              "data_file_keep_cols" : column_name_list,
    #              "meta_file_drop_cols" : column_name_list,
    #              "meta_file_keep_cols" : column_name_list,
    #              "join_on" : column_name
    #              }
    # Any column list above may be empty or False.
    # If a drop column list is empty or False, then no columns will be dropped.
    # If a keeper column list is empty or False, then every column will be kept.
    #
    # "transformations" = {
    #     "log10" : ["geo_mean"]
    # }
    # is a dictionary with operations keying lists of column names. Currently only log10 transformations are implemented.
    #
    # filtering_criteria = {
    #                       "column_name1" : (desired_column_value, operator),
    #                       "column_name2" : (desired_column_value, operator),
    #                        ... }
    # The parameter operator may be "eq" (==), "gteq" (>=), or "lteq" (<=).
    # This dictionary will generally have the "channel" column for flow cytometry data.
    # If the parameter is missing, empty, or False, no filtering will take place.
    #
    # NOTE: Transformations occur BEFORE filtering.
    #
    # control_params = { "pos" : "HIGH_FITC","neg" : "EMPTY_VECTOR", "control_col" : "control_type",
    #                   "circuit_col" : "strain_circuit", "geo_mean_col" : "mean_log10", "geo_mean_log10" : True }
    # Specify the column name indicating the control information ("control_type" in the example) and the column values
    # for the positive and negative controls ("HIGH_FITC" and "EMPTY_VECTOR" in the example).
    # Also specify the column that contains the circuit name (such as "AND", "OR", etc.). In the example the circuit
    # column name is "strain_circuit". Identify the column with the geometric mean of the histograms. Will be
    # used to calculate fold-change in controls. Lastly, state whether or not the geometric mean is in log scale or not.
    # This determines whether a difference or a ratio is computed.
    #
    # NOTE: The "best" controls are going to be chosen across all provided controls. This means that other conditions
    # such as media, temp, etc. will not necessarily match. To get around this currently, break up the input file into
    # conditions that have uniform controls; e.g. by plate.
    #
    # The circuit_params dictionary must contain the values of the circuit input states (these must be passed as strings, but 
    # will be cast to integer for matching to the dataframe entries), the column that contains the input state values, and 
    # a collection of dataframe columns to group over. Example:
    # circuit_params = {
    #                   "input_states" : ["00","01","10","11"],
    #                   "input_col" : "strain_input_state",
    #                   "group_cols" : ["strain_circuit","media_type","timepoint"]
    #                   }
    # group_cols are the groupby columns for performing circuit calculations. In this case,
    # for each combination of time point, strain circuit, and media, a collection of circuits will be formed
    # over the remaining replicates. E.g., if temperature varies, but is not in the group_cols list, then input states across
    # temperatures will be paired together to create full circuits.
    #
    # The optional json file args for saving names allows intermediate results to be captured; e.g. the individual truth table scores, 
    # the experimental conditions for each circuit, and the set of controls used. If not specified, the intermediate results
    # are saved to the current folder and overwritten.

    try:
        config = json.load(open(config_json))
    except:
        config = config_json
    df, bin_vals = datamerge.data_meta_merge(datafile,metafile,config)
    # conditions = get_conditions(df, config["control_params"]["circuit_col"], config["circuit_params"]["group_cols"])
    conditions_per_circuit = get_conditions_per_circuit(df, config["control_params"]["circuit_col"], config["circuit_params"]["group_cols"])
    pos_df, neg_df, circuit_df = separate_controls(df,config["control_params"])
    if len(pos_df) == 0 or len(neg_df) == 0:
        if len(pos_df) == 0 and len(neg_df) == 0:
            message = "positive and negative controls"
        elif len(pos_df) == 0:
            message = "positive controls"
        elif len(neg_df) == 0:
            message = "negative controls"
        message = "There are no {} that satisfy the filtering criteria. Redo with more flexible constraints.".format(message)
        name = os.path.splitext(tt_fname)[0] + "_message.json"
        json.dump("Error: {}".format(message),open(name,"w"))
        raise ValueError(message)
    print("Extracting circuits.")
    circuits = extract_circuits(circuit_df,config["circuit_params"],config["control_params"]["circuit_col"])
    controls = choose_best_controls_wasserstein(pos_df,neg_df,config["file_columns"]["join_on"],config["control_params"]["geo_mean_col"],config["control_params"]["geo_mean_log10"],bin_vals)
    if isinstance(controls,str):
        name = os.path.splitext(tt_fname)[0] + "_message.json"
        json.dump("Error: {}".format(controls),open(name,"w"))
        raise ValueError(controls)
    if controls["fold_change"] < 10:
        print("\nWarning: fold change between positive and negative controls is less than 10.")
    controls["bin_vals"] = bin_vals
    print("Scoring circuits.")
    scores = score_circuits_parallel(circuits,bin_vals,controls)
    print("\nSaving intermediate results.")
    json_scores = {str(key) : val for key,val in scores.items()}
    json.dump(json_scores,open(tt_fname,"w"))
    json.dump(conditions_per_circuit,open(cond_fname,"w"))
    new_controls = {}
    for k,v in controls.items():
        if k in ["pos","neg"]:
            new_controls[k]=v.tolist()
        else:
            new_controls[k] = v
    json.dump(new_controls,open(ctrl_fname,"w"))
    return scores, controls, conditions_per_circuit
