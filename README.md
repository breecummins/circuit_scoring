# Overview

The module `rank_order_truth_tables.py` takes 2<sup>k</sup> flow cytometry histograms from a biologically synthetic genetic network with `k` inputs and ranks all possible truth tables that the data could represent. The ranking is based on the averaged cut score algorithm, which uses the Wasserstein distance between histograms.

`rank_truth_tables` _requires_ positive and negative controls. However, if the constant truth tables are removed from the ranking, then the remaining 2<sup>k<sup>2</sup></sup> - 2 truth tables can be scored without controls. This is not recommended, however can be accessed through the function `rank_noncst_tables` in `rank_order_truth_tables_lib.py`.


# Installation

Dependencies: Anaconda 3 or Miniconda 3

After cloning, do the following.
```bash
$ cd circuit_scoring
$ conda env create -f conda_env.yml
$ conda activate circuit_scoring
$ . install.sh
```

This will `pip` install the python package `circuit_scoring`.

# Commands and Inputs

```bash
$ python call_job.py datafile metafile configfile savefile
```

`datafile` is the file name of a .csv with flow cytometry data that is summarized into a histogram in each row. The datafile must contain `bin_#` columns indicating the bin values of the histogram. There must be a column with the geometric mean of the histogram and one with a unique identifier. 

A string representation of a list of such data files may also be passed, i.e. 
```bash 
"['data1.csv', 'data2.csv', ..., 'dataN.csv']"
``` 
This list of files will be converted into a concatenated dataframe for joint circuit scoring, and not individually scored. Every entry in all files must therefore have a unique identifier. This functionality is intended to handle the case where circuit performance can only be assessed over multiple experiments.

`metafile` is a .csv metadata file, or a string representation of a list of metadata files, associated to `datafile` that can be joined to `datafile` over the unique identifier column. If a list is passed, i.e. 
```bash 
"['metadata1.csv', 'metadata2.csv', ..., 'metadataN.csv']"
```
it must match the order of the list of data files. This list of metadata files will be converted into a concatenated dataframe as above.

`configfile` is a json dictionary file containing the following five dictionaries: `file_columns`, `transformations`, `filtering_criteria`, `control_params`, and `circuit_params`.

`file_columns`: 
```json
{
 "data_file_drop_cols" : column_name_list,
 "data_file_keep_cols" : column_name_list,
 "meta_file_drop_cols" : column_name_list,
 "meta_file_keep_cols" : column_name_list,
 "join_on" : column_name
}
```
All keys are required.

Any column list above may be an empty list or `false`. Generally speaking, either a `drop` list or a `keep` list will be specified for each file, simply because it is sometimes easier to specify what to keep and sometimes easier to specify what to drop. If the `drop` list is specified, then the resulting dataframe will only have columns that are not in the list. Conversely, if the `keep` list is specified, then the resulting dataframe will only have columns that are in the list. If both `drop` and `keep` lists are empty or `false`, then every column will be kept.

`transformations`: 
```json
{
    "log10" : column_name_list
}
```
Keys are optional. If the dictionary is empty, no transformations will be performed.

This dictionary has operations keying lists of column names. Currently only `log10` transformations are implemented, in which every column in the list will have its values log base 10 transformed. 

`filtering_criteria`:
```json
{
  "column_name1" : (desired_column_value, operator),
  "column_name2" : (desired_column_value, operator),
   ... 
}
```
Keys are optional. If this dictionary is empty, no filtering will take place.

The parameter operator may be `"eq"` (==), `"gteq"` (>=), or `"lteq"` (<=). The resulting dataframe will drop all rows where the desired value is not attained.


**NOTE:** Transformations occur BEFORE filtering, so desired values must be log 10 transformed if the data are.

`control_params`:
```json 
{ 
  "pos" : column_value,
  "neg" : column_value, 
  "control_col" : column_name,
  "circuit_col" : column_name, 
  "geo_mean_col" : column_name, 
  "geo_mean_log10" : true or false 
}
```
All keys are required.

`control_col` specifies the column name where positive and negative controls are identified. The value in `pos` is the matching value in `control_col` that indicates that the row in question is data for a positive control. Similarly for `neg` indicating negative control data.

`circuit_col` indicates the column name where the logic circuit under experiment is identified. Example values: "AND", "OR", etc.

`geo_mean_col` is the column name containing the geometric mean of the histograms. `geo_mean_log10` specifies whether or not the geometric mean is in log scale or not. Note that the log10 transformations will already have been carried out before this point, so even if the geometric mean is not log10, this value should be `true` if a log10 transformation is specified. This determines whether a difference or a ratio is computed.

`circuit_params`:
```json 
 {
  "input_states" : list_of_boolean_inputs,
  "input_col" : column_name,
  "group_cols" : column_name_list
  }
```
All keys are required.

`input_states` is a list of the truth table inputs for the circuits under consideration. For example for two inputs, the input states may be `["00","01","10","11"]` or `[0,1,10,11]` depending on the data type in the metadata file. The `input_col` is the name of the column where these inputs can be found.

`group_cols` are the groupby columns for performing circuit calculations. Any collection of column names may be specified here.

# Output

A dataframe `.csv` with summary statistics across the grouped columns.

The column `prop_first_place` is the proportion of times that the experimental circuit performance is best described by the desired truth table. `num_reps` is the number of distinct circuits that were scored across the grouped columns. `mean_rank` and `std_rank` are the mean and standard deviation of the rank (1-9) of the desired truth table. `mean_cut_score ` and `std_cut_score` are the mean and standard deviation of the numerical scores that are used to assign rank to truth tables. Ideally, when the controls are properly positioned with respect to the data, these scores are between zero and one. However, this is not guaranteed for any dataset. 



