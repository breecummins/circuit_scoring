from setuptools import setup, find_packages

setup(name='circuit_scoring',
      version='0.1',
      description='Averaged Cut Wasserstein Measure of Circuit Performance',
      url='https://gitlab.sd2e.org/bcummins/circuit_scoring',
      author='Bree Cummins',
      author_email='breschine.cummins@montana.edu',
      license='MIT',
      packages=find_packages("src", exclude=["circuit_scoring_app-0.1.0"]),
      package_dir={'':'src'},
      zip_safe=False)
