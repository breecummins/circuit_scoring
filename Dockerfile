FROM continuumio/miniconda3:latest

WORKDIR /circuit_scoring
COPY conda_env.yml setup.py README.md ./
ADD src/ ./src/

# from the gitlab ci
RUN ls
RUN apt update
RUN apt-get install --yes gcc --fix-missing
RUN conda env create --file conda_env.yml
RUN [ "/bin/bash", "-c", "source activate circuit_scoring && pip install ." ]
RUN echo "source activate circuit_scoring" > ~/.bashrc
RUN export PYTHONPATH=/circuit_scoring:$PYTHONPATH
